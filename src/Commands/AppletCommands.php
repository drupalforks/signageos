<?php

namespace Drupal\signageos\Commands;

/**
 * A Drush commandfile for signageOS applets.
 */
class AppletCommands extends Base {

  /**
   * Push latest version of the applet to signageOS platform.
   *
   * @usage signageos:applet:push
   *   Pushes the latest applet to the platform.
   *
   * @command signageos:applet:push
   * @aliases sosap
   */
  public function pushApplet() {
    $this->plugin->pushApplet();
  }

}
