<?php

namespace Drupal\signageos\EventSubscriber;

use Drupal\digital_signage_framework\DigitalSignageFrameworkEvents;
use Drupal\digital_signage_framework\Event\CssFiles;
use Drupal\digital_signage_framework\Event\Rendered;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * signageOS event subscriber.
 */
class SignageOs implements EventSubscriberInterface {

  /**
   * @param \Drupal\digital_signage_framework\Event\CssFiles $event
   */
  public function onCssFiles(CssFiles $event): void {
    if ($event->getDevice()->bundle() === 'signageos') {
      $event->addFile(drupal_get_path('module', 'signageos') . '/css/signageos.css');
    }
  }

  /**
   * @param \Drupal\digital_signage_framework\Event\Rendered $event
   */
  public function onRendered(Rendered $event): void {
    if ($event->getDevice()->bundle() === 'signageos') {
      $event->getResponse()->headers->set('Access-Control-Allow-Origin', 'https://hugstatic.blob.core.windows.net');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DigitalSignageFrameworkEvents::CSSFILES => ['onCssFiles'],
      DigitalSignageFrameworkEvents::RENDERED => ['onRendered'],
    ];
  }

}
