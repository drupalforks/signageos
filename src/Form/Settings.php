<?php

namespace Drupal\signageos\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure signageOS settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'signageos_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['signageos.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $this->config('signageos.settings')->get('client_id'),
      '#required' => TRUE,
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#default_value' => $this->config('signageos.settings')->get('client_secret'),
      '#required' => TRUE,
    ];
    $form['welcome_msg'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Welcome message'),
      '#default_value' => $this->config('signageos.settings')->get('welcome_msg'),
      '#required' => TRUE,
    ];
    $form['bg_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background color'),
      '#default_value' => $this->config('signageos.settings')->get('bg_color'),
      '#required' => TRUE,
    ];
    $form['fg_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text color'),
      '#default_value' => $this->config('signageos.settings')->get('fg_color'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('signageos.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('welcome_msg', $form_state->getValue('welcome_msg'))
      ->set('bg_color', $form_state->getValue('bg_color'))
      ->set('fg_color', $form_state->getValue('fg_color'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
