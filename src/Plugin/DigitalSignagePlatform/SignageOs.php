<?php

namespace Drupal\signageos\Plugin\DigitalSignagePlatform;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Serialization\Yaml;
use Drupal\digital_signage_device\DeviceInterface;
use Drupal\digital_signage_device\Entity\Device;
use Drupal\digital_signage_platform\PlatformPluginBase;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Plugin implementation of the digital_signage_platform.
 *
 * @DigitalSignagePlatform(
 *   id = "signageos",
 *   label = @Translation("signageOS"),
 *   description = @Translation("Integrates into signageOS platform.")
 * )
 */
class SignageOs extends PlatformPluginBase {

  public const BASE_URL = 'https://api.signageos.io/v1/';

  public const APPLET_NAME = 'Drupal';

  public const APPLET_VERSION = '1.1.0';

  public const FRONT_APPLET_VERSION = '4.2.0';

  public const DEFAULT_ORIENTATION = 'LANDSCAPE';

  public const DEFAULT_RESOLUTION = 'FULL_HD';

  public const RESOLUTIONS = [
    self::DEFAULT_RESOLUTION => [
      'width' => 1920,
      'height' => 1080,
    ],
    'HD_READY' => [
      'width' => 1080,
      'height' => 720,
    ],
  ];

  public const PORTRAIT_QUERY = 'PORTRAIT';

  /**
   * @var string
   */
  private $clientId;

  /**
   * @var string
   */
  private $clientSecret;

  /**
   * @var string
   */
  private $welcomeMsg;

  /**
   * @var string
   */
  private $bgColor;

  /**
   * @var string
   */
  private $fgColor;

  /**
   * @var array
   */
  private $fonts;

  /**
   * @var array
   */
  private $httpHeader;

  /**
   * {@inheritdoc}
   */
  public function init(): void {
    $ds_config = $this->configFactory->get('digital_signage_framework.settings');
    $sos_config = $this->configFactory->get('signageos.settings');
    $this->clientId = $sos_config->get('client_id');
    $this->clientSecret = $sos_config->get('client_secret');
    $this->welcomeMsg = $sos_config->get('welcome_msg');
    $this->bgColor = $sos_config->get('bg_color');
    $this->fgColor = $sos_config->get('fg_color');

    $this->fonts = Yaml::decode($ds_config->get('fonts')) ?? [];
    $this->httpHeader = Yaml::decode($ds_config->get('http_header')) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function scheduleBaseFields(array &$fields): void {
    $fields['signageos_extid'] = BaseFieldDefinition::create('string')
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setLabel(t('External ID'))
      ->setDescription(t('The external ID of the schedule entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getPlatformDevices(): array {
    $this->messenger->addStatus('Receiving signageOS devices');

    $deviceEntities = [];

    foreach ($this->request('device') as $device) {
      $resolution = $this->getCurrentDeviceResolution($device);

      if ($this->isPortraitOrientation($resolution)) {
        $width = self::RESOLUTIONS[$resolution['resolution']]['height'];
        $height = self::RESOLUTIONS[$resolution['resolution']]['width'];
      }
      else {
        $width = self::RESOLUTIONS[$resolution['resolution']]['width'];
        $height = self::RESOLUTIONS[$resolution['resolution']]['height'];
      }

      $deviceEntity = Device::create([
        'bundle' => $this->getPluginId(),
        'extid' => $device['uid'],
        'title' => $device['name'],
        'status' => TRUE,
        'description' => $device['name'],
        'width' => $width,
        'height' => $height,
      ]);
      $deviceEntities[] = $deviceEntity;

    }

    return $deviceEntities;
  }

  /**
   * @return array
   */
  private function prepareFonts(): array {
    $fonts = [];
    foreach ($this->fonts as $font) {
      if ($font['enabled']) {
        $fonts[] = [
          'uid' => implode('-', [
            $font['family'],
            $font['weight'],
            $font['style'],
            $font['stretch']
          ]),
          'fontFamily' => $font['family'],
          'fontWeight' => $font['weight'],
          'fontStyle' => $font['style'],
          'fontStretch' => $font['stretch'],
          'unicodeRange' => $font['urange'],
          'formats' => $font['formats'],
        ];
      }
    }
    return $fonts;
  }

  /**
   * {@inheritdoc}
   */
  public function pushSchedule(DeviceInterface $device, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE): void {
    $at = $this->dateFormatter->format($this->time->getRequestTime(), 'html_datetime');
    $configuration = $device->getApiSpec($debug, $reload_assets, $reload_content);
    $configuration['fonts'] = $this->prepareFonts();
    $configuration['httpHeader'] = $this->httpHeader;

    $appletUid = $this->getAppletUid();
    $deviceTiming = NULL;
    foreach ($this->request('timing?deviceUid=' . $device->extId(), 'get') as $timing) {
      if ($timing['appletUid'] === $appletUid && $timing['appletVersion'] === self::APPLET_VERSION) {
        $deviceTiming = $timing;
        break;
      }
    }
    if ($deviceTiming !== NULL) {
      if (md5(json_encode($configuration)) !== md5(json_encode($deviceTiming['configuration']))) {
        $timingUid = $deviceTiming['uid'];
        unset($deviceTiming['uid']);
        $deviceTiming['configuration'] = $configuration;
        $this->request('timing/' . $timingUid, 'put', $deviceTiming);
        $this->sendCommandUpdateConfiguration($device, $appletUid, $configuration);
      }
      else {
        $this->sendCommandUpdateSchedule($device, $appletUid);
      }
    }
    else {
      $this->request('timing', 'post', [
        'deviceUid' => $device->extId(),
        'appletUid' => $this->pushApplet(),
        'appletVersion' => self::APPLET_VERSION,
        'configuration' => $configuration,
        'startsAt' => $at,
        'endsAt' => $at,
        'position' => 1,
        'finishEventType' => 'IDLE_TIMEOUT',
        'finishEventData' => 1234567890,
      ]);
      $this->sendPowerAction($device, 'APPLET_RELOAD');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function reloadSchedule(DeviceInterface $device, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE): void {
    $configuration = $device->getApiSpec($debug, $reload_assets, $reload_content);
    $configuration['fonts'] = $this->prepareFonts();
    $configuration['httpHeader'] = $this->httpHeader;
    $appletUid = $this->getAppletUid();
    $this->sendCommandUpdateConfiguration($device, $appletUid, $configuration);
  }

  /**
   * @param $path
   * @param string $method
   * @param array $body
   *
   * @return array|string
   */
  protected function request($path, $method = 'get', $body = []) {
    $headers = [
      'Cache-Control' => 'no-cache',
      'Content-Type' => 'application/json',
      'X-Auth' => implode(':', [
        $this->clientId,
        $this->clientSecret,
      ]),
    ];
    $options['headers'] = $headers;
    if (!empty($body)) {
      $options['body'] = json_encode($body);
    }
    try {
      $client = $this->clientFactory->fromOptions(['base_uri' => self::BASE_URL]);
      $response = $client->request($method, $path, $options);
      $statusCode = $response->getStatusCode();
      if ($statusCode === 200) {
        $content = $response->getBody()->getContents();
        return json_decode($content, TRUE);
      }
    } catch (GuzzleException $ex) {
    }
    return [];
  }

  /**
   * Gets the resolution of the given device.
   *
   * @param $device array|mixed|string
   *
   * @return array
   */
  protected function getCurrentDeviceResolution($device): array {
    $resolutions = $this->request('device/' . $device['uid'] . '/resolution');
    return $this->getLatestSucceededResolution($resolutions);
  }

  /**
   * Gets the latest resolution setting that was successful.
   *
   * @param $resolutions array
   *
   * @return array
   */
  protected function getLatestSucceededResolution($resolutions): array {
    $currentResolution = [
      'resolution' => self::DEFAULT_RESOLUTION,
      'orientation' => self::DEFAULT_ORIENTATION,
    ];
    $latestTimestamp = -1;
    foreach ($resolutions as $resolution) {
      if ($resolution['succeededAt'] === NULL) {
        continue;
      }
      if (strtotime($resolution['createdAt']) > $latestTimestamp) {
        $latestTimestamp = strtotime($resolution['succeededAt']);
        $currentResolution['resolution'] = $resolution['resolution'];
        $currentResolution['orientation'] = $resolution['orientation'];
      }
    }

    return $currentResolution;
  }

  /**
   * Gets whether the orientation is portrait or not.
   *
   * @param array $resolution
   *
   * @return bool
   */
  protected function isPortraitOrientation(array $resolution): bool {
    return strpos($resolution['orientation'], self::PORTRAIT_QUERY) === 0;
  }

  /**
   * @return string
   */
  protected function getAppletUidId(): string {
    return 'applet_uid';
  }

  /**
   * @return string
   */
  protected function getAppletName(): string {
    return self::APPLET_NAME . ': ' . $this->configFactory->get('system.site')
        ->get('name');
  }

  /**
   * @param bool $push
   *
   * @return string|null
   */
  protected function getAppletUid($push = TRUE): ?string {
    $appletUid = $this->configFactory->get('signageos.settings')
      ->get($this->getAppletUidId());
    if (empty($appletUid) && $push) {
      $appletUid = $this->pushApplet();
    }
    return $appletUid;
  }

  /**
   * @return array
   */
  protected function findOrCreateApplet() {
    foreach ($this->request('applet') as $applet) {
      if ($applet['name'] === $this->getAppletName()) {
        return $applet;
      }
    }
    $this->request('applet', 'post', ['name' => $this->getAppletName()]);
    return $this->findOrCreateApplet();
  }

  /**
   * @return string
   */
  public function pushApplet(): string {
    $logo_file = theme_get_setting('logo.url');
    if (pathinfo($logo_file, PATHINFO_EXTENSION) === 'svg') {
      $logo_content = file_get_contents(DRUPAL_ROOT . $logo_file);
      if ($pos = strpos($logo_content, '<svg')) {
        $logo_content = substr($logo_content, $pos);
      }
    }
    else {
      $logo_content = '';
    }

    // Build applet.
    $build = [
      '#theme' => 'signageos_applet',
      '#sitename' => $this->configFactory->get('system.site')->get('name'),
      '#welcome' => $this->welcomeMsg,
      '#bgcolor' => $this->bgColor,
      '#fgcolor' => $this->fgColor,
      '#logo' => $logo_content,
    ];
    $binary = (string) $this->renderer->renderPlain($build);

    // Find or create applet and version
    if ($applet_uid = $this->getAppletUid(FALSE)) {
      $applet = $this->request('applet/' . $applet_uid);
    }
    if (empty($applet)) {
      $applet = $this->findOrCreateApplet();
    }
    $appletRelease = $this->request('applet/' . $applet['uid'] . '/version/' . self::APPLET_VERSION);
    if (empty($appletRelease)) {
      // Create the applet release.
      $this->request('applet/' . $applet['uid'] . '/version', 'post', [
        'binary' => $binary,
        'version' => self::APPLET_VERSION,
        'frontAppletVersion' => self::FRONT_APPLET_VERSION,
      ]);
      $appletRelease = $this->request('applet/' . $applet['uid'] . '/version/' . self::APPLET_VERSION);
      $this->messenger->addStatus('Created new applet ' . $applet['name']);
    }
    else {
      // Update an applet release.
      $this->request('applet/' . $applet['uid'] . '/version/' . self::APPLET_VERSION, 'put', [
        'binary' => $binary,
        'frontAppletVersion' => self::FRONT_APPLET_VERSION,
      ]);
      $this->messenger->addStatus('Updated applet ' . $applet['name']);
    }

    if (empty($applet_uid) || $applet_uid !== $appletRelease['appletUid']) {
      $this->configFactory->getEditable('signageos.settings')
        ->set($this->getAppletUidId(), $appletRelease['appletUid'])
        ->save();
    }

    return $appletRelease['appletUid'];
  }

  public function sendCommandUpdateSchedule(DeviceInterface $device, $applet_uid): void {
    $this->request('device/' . $device->extId() . '/applet/' . $applet_uid . '/command', 'post', [
      'commandPayload' => [
        'payload' => [],
        'type' => 'UpdateSchedule',
      ],
    ]);
  }

  public function sendCommandUpdateConfiguration(DeviceInterface $device, $applet_uid, array $configuration): void {
    $this->request('device/' . $device->extId() . '/applet/' . $applet_uid . '/command', 'post', [
      'commandPayload' => [
        'payload' => $configuration,
        'type' => 'UpdateConfig',
      ],
    ]);
  }

  protected function getLog(DeviceInterface $device, $type) {
    $log = [];
    if ($applet_uid = $this->getAppletUid(FALSE)) {
      foreach ($this->request('device/' . $device->extId() . '/applet/' . $applet_uid . '/command?type=' . $type . '&receivedSince=' . $this->getSince(3600)) as $item) {
        $message = $item['commandPayload']['payload'];
        if (!is_scalar($message)) {
          $message = json_encode($message);
          if (strlen($message) > 80) {
            $this->storeRecord($item['uid'], $item['commandPayload']['payload']);
            $message = substr($message, 0, 70) . '... ' . $item['uid'];
          }
        }
        $log[] = [
          'time' => $item['receivedAt'],
          'payload' => $item['commandPayload']['payload'],
          'uid' => $item['uid'],
          'message' => $message,
        ];
      }
    }
    return $log;
  }

  /**
   * {@inheritdoc}
   */
  public function showDebugLog(DeviceInterface $device) {
    return $this->getLog($device, 'DeviceLog.Debug');
  }

  /**
   * {@inheritdoc}
   */
  public function showErrorLog(DeviceInterface $device) {
    return $this->getLog($device, 'DeviceLog.Error');
  }

  /**
   * {@inheritdoc}
   */
  public function debugDevice(DeviceInterface $device): void {
    $this->request('device/' . $device->extId() . '/debug', 'put', [
      'appletEnabled' => TRUE,
      'nativeEnabled' => TRUE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function sendPowerAction(DeviceInterface $device, $action) {
    $this->request('device/' . $device->extId() . '/power-action', 'post', [
      'devicePowerAction' => $action,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getScreenshot(DeviceInterface $device, $refresh = FALSE): array {
    if ($refresh) {
      $this->request('device/' . $device->extId() . '/screenshot', 'post');
    }
    if ($screenshots = $this->request('device/' . $device->extId() . '/screenshot?takenSince=' . $this->getSince(600))) {
      return array_pop($screenshots);
    }
    return [];
  }

  /**
   * @param int $seconds
   *   Seconds from now into the past.
   *
   * @return string
   *   Formatted datetime string for sOS requests.
   */
  private function getSince($seconds): string {
    $since = $this->dateFormatter->format($this->time->getCurrentTime() - $seconds, 'custom', DATE_RFC3339_EXTENDED, 'utc');
    return str_replace('+00:00', 'Z', $since);
  }

}
